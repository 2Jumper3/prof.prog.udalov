//
//  AppDelegate.swift
//  Market
//
//  Created by Sergey on 20.01.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    let requestFactory = RequestFactory()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        let showComments = requestFactory.showComments()
//        showComments.showAllComments(id_product: 123) { response in
//            switch response.result {
//            case .success(let commentSuccess):
//                print(commentSuccess)
//            case .failure(let error):
//                print(error.localizedDescription)
//            }
//        }
//        let getUserInfo = requestFactory.getUserInfo()
//        getUserInfo.getUserInfo(id_user: 123) { response in
//            switch response.result {
//            case .success(let userInfo):
//                print(userInfo)
//                
//            case .failure(let error):
//                print(error.localizedDescription)
//            }
//        }
        /*
        let auth = requestFactory.makeAuthRequestFatory()
        auth.login(userName: "2Jumper3", password: "mypassword") { response in
            switch response.result {
            case .success(let login):
                print(login)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        let logout = requestFactory.makeLogoutRequest()
        logout.logout(id_user: 123) { response in
            switch response.result {
            case .success(let logout):
                print (logout)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        let register = requestFactory.registerNewUser()
        register.registerNewUser(userName: "2Jumper3", password: "pass") { response in
            switch response.result {
            case .success(let register):
                print(register)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        let getGoods = requestFactory.getGoods()
        getGoods.getNewGoods(pageNumber: 1, id_category: 1) { response in
            switch response.result {
            case .success(let goods):
                print(goods)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        let cardList = requestFactory.getCardInfo()
        cardList.getCardRequest(id_user: 123) { response in
            switch response.result {
                case .success(let cardList):
                    print(cardList)
                case .failure(let error):
                    print(error.localizedDescription)
            }
        }
        
        let newComment = requestFactory.newComment()
        newComment.newCommentPost(id_user: 123, text: "TextForFeedbackComment: 32 row, NewComment.swift") { response in
            switch response.result {
            case .success(let newComment):
                print(newComment)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        let showComments = requestFactory.showComments()
        showComments.showAllComments(id_product: 123) { response in
            switch response.result {
            case .success(let commentSuccess):
                print(commentSuccess)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        
        let addToBasket = requestFactory.addToBasket()
        addToBasket.addToBasket(id_user: 12, id_product: 123, quantity: 1) { response in
            switch response.result {
            case .success(let addToBasket):
                print(addToBasket)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
        */
    return true
    }
    

    


    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

