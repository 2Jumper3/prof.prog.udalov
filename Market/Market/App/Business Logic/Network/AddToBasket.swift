//
//  AddToBasket.swift
//  Market
//
//  Created by Sergey on 06.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import Alamofire

class AddToBasket: AbstractRequestFactory {
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(string: "http://0.0.0.0:8080/")!

    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension AddToBasket: AddToBasketRequestFactory {
    func addToBasket(id_user: Int, id_product: Int, quantity: Int, completionHandler: @escaping (DataResponse<AddToBasketResponse>) -> Void) {
        let requestModel = BasketRequest(baseUrl: baseUrl, id_user: id_user, id_product: id_product, quantity: quantity)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension AddToBasket {
    struct BasketRequest: RequestRouter {
        
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "addToBasket"
         
        let id_user: Int
        let id_product: Int
        let quantity: Int
        
        var parameters: Parameters? {
            return [
                    "id_user": id_user,
                    "id_product": id_product,
                    "quantity": quantity
                   ]
        }
    }
}
