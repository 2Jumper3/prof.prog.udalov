//
//  AuthRequestFactory.swift
//  Market
//
//  Created by Sergey on 20.01.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import Alamofire

protocol AuthRequestFactory {
    func login(userName: String, password: String, completionHandler: @escaping (DataResponse<LoginResult>) -> Void)
}

protocol LogOutRequestFactory {
    func logout (id_user: Int, completionHandler: @escaping (DataResponse<LogOutResult>) -> Void)
}

protocol RegisterRequestFactory {
    func registerNewUser (userName: String, password: String, email: String, gender: String, creditCard: String, bio: String, completionHandler: @escaping (DataResponse<RigesterResult>) -> Void)
}

protocol GetGoodsRequestFactory {
    func getNewGoods (pageNumber: Int, id_category: Int, completionHandler: @escaping (DataResponse<GoodsList>)  -> Void)
}

protocol GetCardRequestFactory {
    func getCardRequest (id_user: Int, completionHandler: @escaping (DataResponse<CardList>)  ->  Void)
}

protocol NewCommentRequestFactory {
    func newCommentPost (id_user: Int, text: String, completionHandler: @escaping (DataResponse<NewCommentResponse>) -> Void)
}

protocol ShowCommentsRequestFactory {
    func showAllComments (id_product: Int, completionHandler: @escaping (DataResponse<AllCommentsResponse>) -> Void)
}
protocol AddToBasketRequestFactory {
    func addToBasket (id_user: Int, id_product: Int, quantity: Int, completionHandler: @escaping (DataResponse<AddToBasketResponse>) -> Void)
}
protocol UserInfoRequestFactory {
    func getUserInfo (id_user: Int, completionHandler: @escaping (DataResponse<UserInfoResponse>) -> Void)
}


