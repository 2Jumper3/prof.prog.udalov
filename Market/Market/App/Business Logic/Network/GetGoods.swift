//
//  GetGoods.swift
//  Market
//
//  Created by Sergey on 21.01.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import Alamofire

class GetGoods: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "http://0.0.0.0:8080/")!

    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension GetGoods: GetGoodsRequestFactory {
    func getNewGoods(pageNumber: Int, id_category: Int, completionHandler: @escaping (DataResponse<GoodsList>) -> Void) {
        let requestModel = Goods(baseUrl: baseUrl, page_number: pageNumber, id_category: id_category)
        self.request(request: requestModel, completionHandler: completionHandler)
        
    }
    
}

extension GetGoods {
    struct Goods: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "getGoods"
        
        let page_number: Int
        let id_category: Int
        var parameters: Parameters? {
            return [
                "page_number": page_number,
                "id_category": id_category,
            ]
        }
    }
}
