//
//  GetUserInfo.swift
//  Market
//
//  Created by Sergey on 04.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import Alamofire

class GetUserInfo: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
//    let baseUrl = URL(string: "https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/")!
    let baseUrl = URL(string: "http://0.0.0.0:8080/")!

    
    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension GetUserInfo: UserInfoRequestFactory {
    func getUserInfo(id_user: Int, completionHandler: @escaping (DataResponse<UserInfoResponse>) -> Void) {
        let requestModel = Login(baseUrl: baseUrl, user_id: id_user)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
    
}

extension GetUserInfo {
    struct Login: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "userinfo"
        
        let user_id: Int
        var parameters: Parameters? {
            return [
                "user_id": user_id
            ]
        }
    }
}
