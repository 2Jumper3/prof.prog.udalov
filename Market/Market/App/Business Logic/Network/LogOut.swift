//
//  LogOut.swift
//  Market
//
//  Created by Sergey on 20.01.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import Alamofire


class Logout: AbstractRequestFactory {
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(string: "http://0.0.0.0:8080/")!
    
    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension Logout: LogOutRequestFactory {
    func logout(id_user: Int, completionHandler: @escaping (DataResponse<LogOutResult>) -> Void) {
        let requestModel = Logout(baseUrl: baseUrl, id_user: id_user)
        self.request(request: requestModel, completionHandler: completionHandler)
        
    }
    
}

extension Logout {
    struct Logout: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "logout"
        let id_user: Int
        
        var parameters: Parameters? {
            return [
                "id_user": id_user,
            ]
        }
    }
}
