//
//  NewComment.swift
//  Market
//
//  Created by Sergey on 05.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import Alamofire



class NewComment: AbstractRequestFactory {
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(string: "http://0.0.0.0:8080/")!

    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension NewComment: NewCommentRequestFactory {
    func newCommentPost(id_user: Int, text: String, completionHandler: @escaping (DataResponse<NewCommentResponse>) -> Void) {
        let requestModel = Comment(baseUrl: baseUrl, id_user: id_user, commentText: text)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension NewComment {
    struct Comment: RequestRouter {
        
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "addComment"
         
        let id_user: Int
        let commentText: String
        var parameters: Parameters? {
            return [
                    "id_user": id_user,
                    "text": commentText
                   ]
        }
    }
}
