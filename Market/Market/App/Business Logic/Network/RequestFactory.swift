//
//  RequestFactory.swift
//  Market
//
//  Created by Sergey on 20.01.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import Alamofire

class RequestFactory {
    
    func makeErrorParser() -> AbstractErrorParser {
        return ErrorParser()
    }
    
    lazy var commonSessionManager: SessionManager = {
        let configuration = URLSessionConfiguration.default
        configuration.httpShouldSetCookies = false
        configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
        let manager = SessionManager(configuration: configuration)
        return manager
    }()
    
    let sessionQueue = DispatchQueue.global(qos: .utility)
    
    func makeAuthRequestFatory() -> AuthRequestFactory {
        let errorParser = makeErrorParser()
        return Auth(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
    
    func makeLogoutRequest() -> LogOutRequestFactory {
        let errorParser = makeErrorParser()
        return Logout(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
    
    func registerNewUser() -> RegisterRequestFactory {
        let errorParser = makeErrorParser()
        return Register(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
    func getGoods() -> GetGoodsRequestFactory {
        let errorParser = makeErrorParser()
        return GetGoods(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
    
    func getCardInfo() -> GetCardRequestFactory {
        let errorParser = makeErrorParser()
        return GetCard(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
    
    func newComment() -> NewCommentRequestFactory {
        let errorParser = makeErrorParser()
        return NewComment(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
    
    func showComments() -> ShowCommentsRequestFactory {
        let errorParser = makeErrorParser()
        return ShowComments(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
    
    func addToBasket() -> AddToBasketRequestFactory {
        let errorParser = makeErrorParser()
        return AddToBasket(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
    func getUserInfo() -> UserInfoRequestFactory {
        let errorParser = makeErrorParser()
        return GetUserInfo(errorParser: errorParser, sessionManager: commonSessionManager, queue: sessionQueue)
    }
    
}
