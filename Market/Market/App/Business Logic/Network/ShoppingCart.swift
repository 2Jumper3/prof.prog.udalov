//
//  ShoppingCart.swift
//  Market
//
//  Created by Sergey on 21.01.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import Alamofire

class GetCard: AbstractRequestFactory {
    let errorParser: AbstractErrorParser
    let sessionManager: SessionManager
    let queue: DispatchQueue?
    let baseUrl = URL(string: "http://0.0.0.0:8080/")!

    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension GetCard: GetCardRequestFactory {
    func getCardRequest(id_user: Int, completionHandler: @escaping (DataResponse<CardList>) -> Void) {
        let requestModel = Card(baseUrl: baseUrl, id_user: id_user)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension GetCard {
    struct Card: RequestRouter {
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "getBasket"
        
        let id_user: Int
        var parameters: Parameters? {
            return [
                "id_user": id_user,
            ]
        }
    }
}
