//
//  ShowComments.swift
//  Market
//
//  Created by Sergey on 06.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import Alamofire



class ShowComments: AbstractRequestFactory {
    var errorParser: AbstractErrorParser
    var sessionManager: SessionManager
    var queue: DispatchQueue?
    let baseUrl = URL(string: "http://0.0.0.0:8080/")!

    init(
        errorParser: AbstractErrorParser,
        sessionManager: SessionManager,
        queue: DispatchQueue? = DispatchQueue.global(qos: .utility)) {
        self.errorParser = errorParser
        self.sessionManager = sessionManager
        self.queue = queue
    }
}

extension ShowComments: ShowCommentsRequestFactory {
    func showAllComments(id_product: Int, completionHandler: @escaping (DataResponse<AllCommentsResponse>) -> Void) {
        let requestModel = Comment(baseUrl: baseUrl, id_product: id_product)
        self.request(request: requestModel, completionHandler: completionHandler)
    }
}

extension ShowComments {
    struct Comment: RequestRouter {
        
        let baseUrl: URL
        let method: HTTPMethod = .post
        let path: String = "showComments"
         
        let id_product: Int
        var parameters: Parameters? {
            return [
                    "id_product": id_product,
                   ]
        }
    }
}
