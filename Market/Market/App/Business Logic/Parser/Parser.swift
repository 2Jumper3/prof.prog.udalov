//
//  Parser.swift
//  Market
//
//  Created by Sergey on 02.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation

class Parser {

static func parseGoods(data: Data?) -> GoodsList? {
    guard let data = data else {
        return nil
    }
    do {
        let decoder = JSONDecoder()
        let response: GoodsList = try decoder.decode(GoodsList.self, from: data)
        return response
    } catch {
        print("JSONDecoder exception \(#file) \(#function) \(#line) \(error)")
    }
    return nil
}
}
