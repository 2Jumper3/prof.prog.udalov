//
//  MainCordinator.swift
//  Market
//
//  Created by Sergey on 19.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
//        let mainVC = LoginViewController()
        
        let mainVC = ProductViewController()
//
//        let layout = UICollectionViewFlowLayout()
        
//        let mainVC = CommentsCollectionViewController(collectionViewLayout: layout)
        mainVC.coordinator = self
        navigationController.pushViewController(mainVC, animated: true)
    }
    
}
