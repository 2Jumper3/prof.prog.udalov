//
//  LoginResult.swift
//  Market
//
//  Created by Sergey on 20.01.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation


struct LoginResult: Codable {
    let result: Int
    let user: User
}


struct LogOutResult: Codable {
    let result: Int
    let userMessage: String
    
}

struct RigesterResult: Codable {
    let result: Int
    let userMessage: String
    
}

struct GoodsList1: Codable {
    let id: Int
    let product_name: String
    let price: Int
    
    
    enum CodingKeys: String, CodingKey {
        case id = "id_product"
        case product_name = "product_name"
        case price = "price"
    }
}
// MARK: - Product

struct GoodsList: Codable {
    let page_number: Int
    let products: [Product]
}

struct Product: Codable {
    let product_name: String
    let id: Int
    let price: Int

    enum CodingKeys: String, CodingKey {
        case product_name = "product_name"
        case id = "id_product"
        case price = "price"
    }
}

struct CardList: Codable {
    let amount: Int
    let countGoods: Int
    let contents: [Contents]
}

struct Contents: Codable {
    let id_product: Int
    let product_name: String
    let price: Int
    let quantity: Int
    
        enum CodingKeys: String, CodingKey {
            case id_product = "id_product"
            case product_name = "product_name"
            case price = "price"
            case quantity = "quantity"
        }
}

struct NewCommentResponse: Codable {
    let result: Int
    let userMessage: String
}

struct AllCommentsResponse: Codable {
    let comments: [Comment]
}

struct Comment: Codable {
    let name: String
    let comment: String
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case comment = "comment"
    }
}

struct AddToBasketResponse: Codable {
    let text: String
}

struct UserInfoResponse: Codable {
    let id: Int
    let login: String
    let name: String
    let lastname: String
    let bio: String
    let credit_card: String
    
    enum CodingKeys: String, CodingKey {
        case id = "id_user"
        case login = "user_login"
        case name = "user_name"
        case lastname = "user_lastname"
        case bio = "bio"
        case credit_card = "credit_card"
}
}
