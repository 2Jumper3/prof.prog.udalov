//
//  Errors.swift
//  Market
//
//  Created by Sergey on 27.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation

enum TableSettingsErrors: Error {
    case numberOfRowsInSection
    case cellForRowAt
}
