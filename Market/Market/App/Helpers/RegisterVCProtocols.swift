//
//  RegisterVCProtocols.swift
//  Market
//
//  Created by Sergey on 19.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import UIKit


//MARK: - делегат для обработки нажатия кнопки
protocol BtnActionLoginDelegate {
    func login ()
}
protocol BtnActionRegisterDelegate {
    func register ()
}

protocol BtnActionOpenRegisterDelegate {
    func openRegisterVC()
}

protocol GenderSegmentControlProtocol {
    func genderChaged(sender: UISegmentedControl)
}

protocol SavePersonalDataProtocol {
    func save()
}
protocol EditViewOpenProtocol {
    func editMainInfo()
}

protocol TableViewLoadingProtocol {
    func download()
}
