//
//  RegisterView.swift
//  Market
//
//  Created by Sergey on 19.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class LoginView: UIView {
    
    var loginDelegate: BtnActionLoginDelegate?
    var registerDelegate: BtnActionOpenRegisterDelegate?
    
    private(set) lazy var headerDescription: UILabel = {
        let description = UILabel()
        description.translatesAutoresizingMaskIntoConstraints = false
        description.textColor = .black
        description.font = UIFont.boldSystemFont(ofSize: 20)
        description.numberOfLines = 50
        return description
    }()
    
    private(set) lazy var userNameTextField: ShakingAnimation = {
        let textField = ShakingAnimation()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "UserName"
        return textField
    }()
    
    private(set) lazy var passwordTextField: ShakingAnimation = {
        let textField = ShakingAnimation()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "Password"
        return textField
    }()
    
    private(set) lazy var  loginBtn: UIButton =  {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Login", for: .normal)
        btn.tintColor = .black
        btn.addTarget(self, action: #selector(btnTapped), for: .touchUpInside)
        btn.backgroundColor = .red
        return btn
    }()
    
    private(set) lazy var  registerBtn: UIButton =  {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Register", for: .normal)
        btn.addTarget(self, action: #selector(openRegisterVC), for: .touchUpInside)
        btn.tintColor = .black
        btn.backgroundColor = .red
        return btn
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupLayout()
    }
    //MARK: - Constrains

    private func setupLayout() {
        self.addSubview(headerDescription)
        self.addSubview(userNameTextField)
        self.addSubview(passwordTextField)
        self.addSubview(loginBtn)
        self.addSubview(registerBtn)
        self.backgroundColor = .blue
        
        let topAnchorContraint: CGFloat = 200
        let widthAnchorContraint: CGFloat = 200
        let spacingContraint: CGFloat = 15
        let heightAnchorContraint: CGFloat = 20
        
        
        NSLayoutConstraint.activate([
            self.headerDescription.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: topAnchorContraint),
            self.headerDescription.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.headerDescription.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.headerDescription.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.userNameTextField.topAnchor.constraint(equalTo: self.headerDescription.bottomAnchor, constant: spacingContraint),
            self.userNameTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.userNameTextField.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.userNameTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.passwordTextField.topAnchor.constraint(equalTo: self.userNameTextField.bottomAnchor, constant: spacingContraint),
            self.passwordTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.passwordTextField.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.passwordTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.loginBtn.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: spacingContraint),
            self.loginBtn.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.loginBtn.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.loginBtn.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.registerBtn.topAnchor.constraint(equalTo: self.loginBtn.bottomAnchor, constant: spacingContraint),
            self.registerBtn.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.registerBtn.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.registerBtn.heightAnchor.constraint(equalToConstant: heightAnchorContraint),

        ])
    }
    
    @objc func btnTapped() {
        loginDelegate?.login()
    }
    @objc func openRegisterVC() {
        registerDelegate?.openRegisterVC()
    }
    
}
