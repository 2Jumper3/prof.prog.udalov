//
//  RegisterViewController.swift
//  Market
//
//  Created by Sergey on 19.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    lazy var loginVC = LoginViewControllerModel()
    weak var coordinator: MainCoordinator?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .red
        addVKVC()
        
    }
    
    private func addVKVC() {
        self.addChild(self.loginVC)
        self.view.addSubview(self.loginVC.view)
        self.loginVC.didMove(toParent: self)
        self.loginVC.view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            self.loginVC.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.loginVC.view.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
            self.loginVC.view.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            self.loginVC.view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        
        ])
    }

    
}
