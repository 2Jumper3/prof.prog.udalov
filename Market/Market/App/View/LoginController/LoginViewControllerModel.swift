//
//  RegisterViewController.swift
//  Market
//
//  Created by Sergey on 19.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class LoginViewControllerModel: UIViewController, UITextFieldDelegate {
    
    weak var coordinator: MainCoordinator?
    
    let requestFactory = RequestFactory()

    private var loginView: LoginView  {
        return self.view as! LoginView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loginView.loginDelegate = self
        self.loginView.registerDelegate = self
        self.loginView.userNameTextField.delegate = self
        self.loginView.passwordTextField.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func loadView() {
        super.loadView()
        self.view = LoginView()
    }
    
}

extension LoginViewControllerModel: BtnActionLoginDelegate {
    func login() {
//        guard let username = self.loginView.userNameTextField.text, !username.isEmpty else {return self.loginView.userNameTextField.shake()}
//        guard let password = self.loginView.passwordTextField.text, !password.isEmpty else {return self.loginView.passwordTextField.shake()}

        
        let auth = requestFactory.makeAuthRequestFatory()
        
        auth.login(userName: String("username"), password: String("password")) { response in
            switch response.result {
            case .success(let login):
                DispatchQueue.main.async {
                let vc = MainViewTabBar()
                self.navigationController?.pushViewController(vc, animated: true)
                print(login)
                }
            case .failure(let error):
                DispatchQueue.main.async {
                let vc = MainViewTabBar()
                self.navigationController?.pushViewController(vc, animated: true)
                }
                print(error.localizedDescription)
            }
        }
    }
}

extension LoginViewControllerModel: BtnActionOpenRegisterDelegate {
    func openRegisterVC() {
        let vc = RegisterViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
