//
//  RegisterModel.swift
//  Market
//
//  Created by Sergey on 20.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import UIKit

class RegisterModel: UIViewController {
    
    weak var coordinator: MainCoordinator?
    
    let requestFactory = RequestFactory()

    private var registerView: RegisterView  {
        return self.view as! RegisterView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.registerView.delegate = self
        self.registerView.segmentControlDelegate = self
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func loadView() {
        super.loadView()
        self.view = RegisterView()
    }
}


extension RegisterModel: BtnActionRegisterDelegate {
    
    func register() {
        guard let username =    self.registerView.userNameTextField.text else {return}
        guard let password =    self.registerView.passwordTextField.text else {return}
        guard let email =       self.registerView.emailTextField.text else {return}
        let gender =      self.registerView.gender
        guard let creditCard =  self.registerView.creditCardTextField.text else {return}
        guard let bio =         self.registerView.bioTextField.text else {return}
        
        let register = requestFactory.registerNewUser()
        register.registerNewUser(userName: username, password: password, email: email, gender: gender, creditCard: creditCard, bio: bio) { response in
            switch response.result {
            case .success(let register):
                print(register)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension RegisterModel: GenderSegmentControlProtocol {
    
    func genderChaged(sender: UISegmentedControl) {
        
            let segmentIndex = sender.selectedSegmentIndex
            let genderType = ["Woman", "Man"]
            self.registerView.gender = genderType[segmentIndex]
            print(genderType[segmentIndex])
            print(self.registerView.gender)
        }
    }
