//
//  RegisterViewController.swift
//  Market
//
//  Created by Sergey on 20.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    lazy var registerVC = RegisterModel()
    weak var coordinator: MainCoordinator?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .red
        addRegisterVC()
    }
    
    private func addRegisterVC() {
        self.addChild(self.registerVC)
        self.view.addSubview(self.registerVC.view)
        self.registerVC.didMove(toParent: self)
        self.registerVC.view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            self.registerVC.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.registerVC.view.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
            self.registerVC.view.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            self.registerVC.view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        
        ])
    }


}
