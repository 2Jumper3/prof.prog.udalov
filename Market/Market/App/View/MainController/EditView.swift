//
//  MainView.swift
//  Market
//
//  Created by Sergey on 25.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class EditView: UIView {
    
    var delegate: SavePersonalDataProtocol?
    var segmentControlDelegate: GenderSegmentControlProtocol?
    
    private(set) lazy var userNameTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "UserName"
        return textField
    }()
    
    private(set) lazy var passwordTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "PassWord"
        return textField
    }()
    
    private(set) lazy var emailTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "e-mail"
        return textField
    }()
    
    private(set) lazy var creditCardTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "Enter Your Credit Card"
        return textField
    }()
    
    private(set) lazy var bioTextField: UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.placeholder = "Biography"
        return textField
    }()
    
    private(set) lazy var genderType: UISegmentedControl = {
        let gender = UISegmentedControl(items: ["Woman", "Man"])
        gender.addTarget(self, action: #selector(indexChanged), for: .valueChanged)
        gender.translatesAutoresizingMaskIntoConstraints = false
        gender.selectedSegmentIndex = 0
        gender.backgroundColor = .gray
        return gender
    }()
    
    private(set) lazy var  registerBtn: UIButton =  {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Save", for: .normal)
        btn.tintColor = .black
        btn.addTarget(self, action: #selector(btnTapped), for: .touchUpInside)
        btn.backgroundColor = .red
        return btn
    }()
    
    var gender: String = {
        let gender = String()
        return gender
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupLayout()
    }
    //MARK: - Constrains
    private func setupLayout() {
        self.addSubview(userNameTextField)
        self.addSubview(passwordTextField)
        self.addSubview(emailTextField)
        self.addSubview(creditCardTextField)
        self.addSubview(bioTextField)
        self.addSubview(genderType)
        self.addSubview(registerBtn)
        
        self.backgroundColor = .blue
        
        let topAnchorContraint: CGFloat = 200
        let widthAnchorContraint: CGFloat = 200
        let spacingContraint: CGFloat = 15
        let heightAnchorContraint: CGFloat = 20
        
        NSLayoutConstraint.activate([
            self.userNameTextField.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: topAnchorContraint),
            self.userNameTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.userNameTextField.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.userNameTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.passwordTextField.topAnchor.constraint(equalTo: self.userNameTextField.bottomAnchor, constant: spacingContraint),
            self.passwordTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.passwordTextField.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.passwordTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.emailTextField.topAnchor.constraint(equalTo: self.passwordTextField.bottomAnchor, constant: spacingContraint),
            self.emailTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.emailTextField.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.emailTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.creditCardTextField.topAnchor.constraint(equalTo: self.emailTextField.bottomAnchor, constant: spacingContraint),
            self.creditCardTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.creditCardTextField.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.creditCardTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.bioTextField.topAnchor.constraint(equalTo: self.creditCardTextField.bottomAnchor, constant: spacingContraint),
            self.bioTextField.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.bioTextField.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.bioTextField.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.genderType.topAnchor.constraint(equalTo: self.bioTextField.bottomAnchor, constant: spacingContraint),
            self.genderType.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.genderType.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.genderType.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
            
            self.registerBtn.topAnchor.constraint(equalTo: self.genderType.bottomAnchor, constant: spacingContraint),
            self.registerBtn.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
            self.registerBtn.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
            self.registerBtn.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
        ])
    }
    
    @objc func btnTapped() {
        delegate?.save()
    }
    
    @objc func indexChanged() {
        segmentControlDelegate?.genderChaged(sender: genderType)
    }
}
