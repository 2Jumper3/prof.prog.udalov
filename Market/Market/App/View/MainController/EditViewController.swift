//
//  EditViewController.swift
//  Market
//
//  Created by Sergey on 04.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class EditViewController: UIViewController {

    lazy var editViewVC = EditViewModel()
    weak var coordinator: MainCoordinator?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .red
        addEditVC()
        
    }
    
    private func addEditVC() {
        self.addChild(self.editViewVC)
        self.view.addSubview(self.editViewVC.view)
        self.editViewVC.didMove(toParent: self)
        self.editViewVC.view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            self.editViewVC.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.editViewVC.view.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
            self.editViewVC.view.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            self.editViewVC.view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}
