//
//  MainViewModel.swift
//  Market
//
//  Created by Sergey on 25.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit
import Foundation

class EditViewModel: UIViewController {
    
    
    weak var coordinator: MainCoordinator?
    
    let requestFactory = RequestFactory()
    
    private var editView: EditView  {
        return self.view as! EditView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.editView.delegate = self
        self.editView.segmentControlDelegate = self

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func loadView() {
        super.loadView()
        self.view = EditView()
    }
}

extension EditViewModel: SavePersonalDataProtocol {
    
    func save() {
        guard let username =    self.editView.userNameTextField.text else {return}
        guard let password =    self.editView.passwordTextField.text else {return}
        guard let email =       self.editView.emailTextField.text else {return}
        let gender =      self.editView.gender
        guard let creditCard =  self.editView.creditCardTextField.text else {return}
        guard let bio =         self.editView.bioTextField.text else {return}
        
        
        
        
        let register = requestFactory.registerNewUser()
        register.registerNewUser(userName: username, password: password, email: email, gender: gender, creditCard: creditCard, bio: bio) { response in
            switch response.result {
            case .success(let register):
                print(register)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}

extension EditViewModel: GenderSegmentControlProtocol {
    
    func genderChaged(sender: UISegmentedControl) {
        
            let segmentIndex = sender.selectedSegmentIndex
            let genderType = ["Woman", "Man"]
            self.editView.gender = genderType[segmentIndex]
            print(genderType[segmentIndex])
            print(self.editView.gender)
    }
}

