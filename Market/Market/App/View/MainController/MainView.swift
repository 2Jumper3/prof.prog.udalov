//
//  MainView.swift
//  Market
//
//  Created by Sergey on 26.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class MainView: UIView {

    var openEditViewDelegate: EditViewOpenProtocol?
    
    private(set) lazy var userNameLabel: UILabel = {
        let textField = UILabel()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.text = "123"

        textField.textAlignment = .center
        return textField
    }()
    
    private(set) lazy var idUserLabel: UILabel = {
        let textField = UILabel()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.text = "123"
        textField.textAlignment = .center
        return textField
    }()
    
    private(set) lazy var emailLabel: UILabel = {
        let textField = UILabel()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.textAlignment = .center
        textField.text = "123"

        return textField
    }()
    
    private(set) lazy var bioLabel: UILabel = {
        let textField = UILabel()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.textAlignment = .center
        textField.text = "123"

        return textField
    }()
    
    private(set) lazy var creditCardLabel: UILabel = {
        let textField = UILabel()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.font = UIFont.systemFont(ofSize: 10)
        textField.textAlignment = .center
        textField.text = "123"

        return textField
    }()
    
    private(set) lazy var  editBtn: UIButton =  {
        let btn = UIButton()
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.setTitle("Edit", for: .normal)
        btn.addTarget(self, action: #selector(openEditVC), for: .touchUpInside)
        btn.tintColor = .black
        btn.backgroundColor = .red
        return btn
    }()
    
    
       override init(frame: CGRect) {
           super.init(frame: frame)
           self.setupLayout()
       }
       
       required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           self.setupLayout()
       }
       //MARK: - Constrains
    private func setupLayout() {
        
           self.addSubview(userNameLabel)
           self.addSubview(idUserLabel)
           self.addSubview(emailLabel)
           self.addSubview(bioLabel)
           self.addSubview(creditCardLabel)
           
        self.addSubview(editBtn)

           self.backgroundColor = .white
           
           let topAnchorContraint: CGFloat = 200
           let widthAnchorContraint: CGFloat = 200
           let spacingContraint: CGFloat = 15
           let heightAnchorContraint: CGFloat = 20
           
           NSLayoutConstraint.activate([
               self.userNameLabel.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: topAnchorContraint),
               self.userNameLabel.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
               self.userNameLabel.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
               self.userNameLabel.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
               
               self.idUserLabel.topAnchor.constraint(equalTo: self.userNameLabel.bottomAnchor, constant: spacingContraint),
               self.idUserLabel.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
               self.idUserLabel.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
               self.idUserLabel.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
               
               self.emailLabel.topAnchor.constraint(equalTo: self.idUserLabel.bottomAnchor, constant: spacingContraint),
               self.emailLabel.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
               self.emailLabel.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
               self.emailLabel.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
               
               self.bioLabel.topAnchor.constraint(equalTo: self.emailLabel.bottomAnchor, constant: spacingContraint),
               self.bioLabel.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
               self.bioLabel.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
               self.bioLabel.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
               
               self.creditCardLabel.topAnchor.constraint(equalTo: self.bioLabel.bottomAnchor, constant: spacingContraint),
               self.creditCardLabel.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
               self.creditCardLabel.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
               self.creditCardLabel.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
               
               self.editBtn.topAnchor.constraint(equalTo: self.creditCardLabel.bottomAnchor, constant: spacingContraint),
               self.editBtn.widthAnchor.constraint(equalToConstant: widthAnchorContraint),
               self.editBtn.centerXAnchor.constraint(equalTo: self.safeAreaLayoutGuide.centerXAnchor),
               self.editBtn.heightAnchor.constraint(equalToConstant: heightAnchorContraint),
               
           ])
       }
    
    @objc func openEditVC() {
        openEditViewDelegate?.editMainInfo()
    }
    

}
