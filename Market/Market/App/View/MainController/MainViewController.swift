//
//  MainViewController.swift
//  Market
//
//  Created by Sergey on 20.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    lazy var mainViewVC = MainViewModel()
    weak var coordinator: MainCoordinator?
    
    let requestFactory = RequestFactory()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Profile"
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .red
        addMainVC()
        
    }
    
    
    private func addMainVC() {
        self.addChild(self.mainViewVC)
        self.view.addSubview(self.mainViewVC.view)
        self.mainViewVC.downloadUserInfo()
        self.mainViewVC.didMove(toParent: self)
        self.mainViewVC.view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            self.mainViewVC.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.mainViewVC.view.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
            self.mainViewVC.view.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            self.mainViewVC.view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
}
