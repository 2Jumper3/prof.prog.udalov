//
//  MainViewModel.swift
//  Market
//
//  Created by Sergey on 25.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class MainViewModel: UIViewController {

    weak var coordinator: MainCoordinator?
    var userDataResponse: UserInfoResponse?
    
    let requestFactory = RequestFactory()

    private var mainView: MainView  {
        return self.view as! MainView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.mainView.openEditViewDelegate = self


    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func loadView() {
        super.loadView()
        self.view = MainView()
    }
    func downloadUserInfo() {
        let getUserInfo = requestFactory.getUserInfo()
        getUserInfo.getUserInfo(id_user: 123) { response in
            switch response.result {
            case .success(let userInfo):
                self.userDataResponse = userInfo
                guard let user = self.userDataResponse else {
                    return
                }
                DispatchQueue.main.async {
                self.mainView.textInput(user.name, user.id, user.lastname, user.bio, user.credit_card)

                }
                print(userInfo)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }


}

extension MainView  {
    func textInput (_ name: String, _ id: Int, _ email: String, _ bio: String, _ creditCard: String ) {
        self.userNameLabel.text = name
        self.idUserLabel.text = String(id)
        self.emailLabel.text = email
        self.bioLabel.text = bio
        self.creditCardLabel.text = creditCard
        

    }
}

extension MainViewModel: EditViewOpenProtocol {
    func editMainInfo() {
        let vc = EditViewController()
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
}
