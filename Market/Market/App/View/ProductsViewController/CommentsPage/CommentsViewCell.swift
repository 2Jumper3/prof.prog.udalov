//
//  CommentsViewCell.swift
//  Market
//
//  Created by Sergey on 10.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class CommentsViewCellMain: BaseCell, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let cellId = "commentsViewCell"
    let downloaderVC = DescriptionAndCommentsViewController()
    var allComments: [Comment] = []
    
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        cv.dataSource = self
        cv.delegate = self
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    func getComments() {
        downloaderVC.getComments { (response) in
            self.allComments = response
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    override func setupViews() {
        super.setupViews()
        getComments()
        self.collectionView.register(CommentsViewCell.self, forCellWithReuseIdentifier: cellId)
        self.addSubview(collectionView)
        
        
        NSLayoutConstraint.activate([
            self.collectionView.topAnchor       .constraint(equalTo: self.topAnchor),
            self.collectionView.leftAnchor      .constraint(equalTo: self.leftAnchor),
            self.collectionView.rightAnchor     .constraint(equalTo: self.rightAnchor),
            self.collectionView.bottomAnchor    .constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.allComments.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! CommentsViewCell
        cell.titleLabel.text = self.allComments[indexPath.row].name
        cell.subTitleLabel.text = self.allComments[indexPath.row].comment
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width, height: 100)
    }


}

class CommentsViewCell: BaseCell {
    

    
    let userProfileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.backgroundColor = .darkGray
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .purple
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let subTitleLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .green
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    
    
    override func setupViews() {
        super.setupViews()
        self.addSubview(userProfileImageView)
        self.addSubview(titleLabel)
        self.addSubview(subTitleLabel)
        
        let widthAnchorUserProfile: CGFloat = 50
        
        NSLayoutConstraint.activate([
            self.userProfileImageView.leftAnchor        .constraint(equalTo: self.leftAnchor, constant: 5),
            self.userProfileImageView.topAnchor         .constraint(equalTo: self.topAnchor, constant: 5),
            self.userProfileImageView.bottomAnchor      .constraint(equalTo: self.bottomAnchor, constant: -5),
            self.userProfileImageView.widthAnchor       .constraint(equalToConstant: widthAnchorUserProfile),
            
            self.titleLabel.topAnchor                   .constraint(equalTo: self.topAnchor, constant: 5),
            self.titleLabel.leftAnchor                  .constraint(equalTo: self.userProfileImageView.rightAnchor, constant: 5),
            self.titleLabel.rightAnchor                 .constraint(equalTo: self.rightAnchor, constant: -5),
            self.titleLabel.heightAnchor                .constraint(equalToConstant: self.frame.size.height / 2),
            
            self.subTitleLabel.topAnchor                .constraint(equalTo: self.titleLabel.bottomAnchor, constant: 5),
            self.subTitleLabel.leftAnchor               .constraint(equalTo: self.titleLabel.leftAnchor),
            self.subTitleLabel.rightAnchor              .constraint(equalTo: self.titleLabel.rightAnchor, constant: -5),
            self.subTitleLabel.heightAnchor             .constraint(equalToConstant: self.frame.size.height / 2)
            
        ])
    }
}
