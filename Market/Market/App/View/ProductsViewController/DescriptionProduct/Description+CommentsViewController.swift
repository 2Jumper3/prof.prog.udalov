//
//  Description+CommentsViewController.swift
//  Market
//
//  Created by Sergey on 10.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

private let reuseIdentifier = "commentsCell"
private let commentsID = "commentsID"
private let descriptionID = "descriptionID"

class DescriptionAndCommentsViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    weak var coordinator: MainCoordinator?
//    let descriptionProductView = ProductDescription()
    let requestFactory = RequestFactory()
    var allComments: [Comment] = []
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionViewSettings()
    }
    
    func getComments(completion: @escaping ([Comment]) -> ()) {
        let showComments = requestFactory.showComments()
        showComments.showAllComments(id_product: 123) { response in
            switch response.result {
            case .success(let commentResponse):
                for index in commentResponse.comments {
                    let comment = Comment(name: index.name, comment: index.comment)
                    self.allComments.append(comment)
                    completion(self.allComments)
                    
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
    // MARK: - UICollectionViewDataSource

    
    func collectionViewSettings()  {
        collectionView?.backgroundColor = .white
        collectionView?.isScrollEnabled = false
        collectionView?.register(CommentsViewCellMain.self, forCellWithReuseIdentifier: commentsID)
        collectionView?.register(DescriptionProductCell.self, forCellWithReuseIdentifier: descriptionID)
        
        if let flowLayout = collectionView?.collectionViewLayout as? UICollectionViewFlowLayout{
            flowLayout.scrollDirection = .horizontal
            
            flowLayout.minimumLineSpacing = 0
        }
        collectionView?.isPagingEnabled = true
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let id: String
        if indexPath.item == 0 {
            id = descriptionID
        } else {
            id = commentsID
            
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: id, for: indexPath)
        
        return cell
    }
    
    
    func scrollToMenuIndex(menuIndex: Int) {
        let indexPath = NSIndexPath(item: menuIndex, section: 0)
        collectionView?.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
    }
}
