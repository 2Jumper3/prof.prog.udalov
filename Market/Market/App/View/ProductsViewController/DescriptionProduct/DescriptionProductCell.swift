//
//  DescriptionProductCell.swift
//  Market
//
//  Created by Sergey on 11.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class DescriptionProductCell: BaseCell {

    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 10)
        label.textColor = .black
        label.font = UIFont.boldSystemFont(ofSize: 20)
        label.numberOfLines = 50
        label.backgroundColor = .green
        label.text = "Here will be plased product description"
        label.textAlignment = .center
        return label
    }()
    
    override func setupViews() {
        super.setupViews()
        self.addSubview(descriptionLabel)
        
        NSLayoutConstraint.activate([
            self.descriptionLabel.topAnchor     .constraint(equalTo: self.topAnchor),
            self.descriptionLabel.leftAnchor    .constraint(equalTo: self.leftAnchor),
            self.descriptionLabel.widthAnchor   .constraint(equalTo: self.widthAnchor),
            self.descriptionLabel.heightAnchor  .constraint(equalTo: self.heightAnchor)
        ])
    }
}
