//
//  DescriptionProduct.swift
//  Market
//
//  Created by Sergey on 10.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class ProductDescription: UIView, UIScrollViewDelegate {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupLayout()
    }
    
    private(set) lazy var scrollView: UIScrollView = {
           let scrollview = UIScrollView()
           scrollview.translatesAutoresizingMaskIntoConstraints = false
           scrollview.delegate = self
           scrollview.isPagingEnabled = true
           return scrollview
           
       }()
       
       private(set) lazy var contentView: UIView = {
           let view = UIView()
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
       
       
       private(set) lazy var blueView: UIView = {
           let view = UIView()
           view.backgroundColor = .blue
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
       
       
       private(set) lazy var productDescription: UILabel = {
           let label = UILabel()
           label.translatesAutoresizingMaskIntoConstraints = false
           label.font = UIFont.systemFont(ofSize: 10)
           label.textColor = .black
           label.font = UIFont.boldSystemFont(ofSize: 20)
           label.numberOfLines = 50
           label.backgroundColor = .lightGray
           label.text = "Here will be plased product description"
           label.textAlignment = .center
           return label
       }()
    
    private func setupLayout() {
        self.addSubview(scrollView)
        
        scrollView.addSubview(contentView)
        contentView.addSubview(productDescription)
        contentView.addSubview(blueView)
        
        NSLayoutConstraint.activate([
            
        self.scrollView.leadingAnchor    .constraint(equalTo: self.leadingAnchor),
        self.scrollView.trailingAnchor   .constraint(equalTo: self.trailingAnchor),
        self.scrollView.topAnchor        .constraint(equalTo: self.topAnchor),
        self.scrollView.bottomAnchor     .constraint(equalTo: self.bottomAnchor),

        
        contentView.leadingAnchor   .constraint(equalTo: scrollView.leadingAnchor),
        contentView.trailingAnchor  .constraint(equalTo: scrollView.trailingAnchor),
        contentView.widthAnchor     .constraint(equalToConstant: UIScreen.main.bounds.width),
        contentView.topAnchor       .constraint(equalTo: scrollView.topAnchor),
        contentView.bottomAnchor    .constraint(equalTo: scrollView.bottomAnchor),

        productDescription.leadingAnchor       .constraint(equalTo: contentView.leadingAnchor),
        productDescription.trailingAnchor      .constraint(equalTo: contentView.trailingAnchor),
        productDescription.topAnchor           .constraint(equalTo: contentView.topAnchor),
        productDescription.heightAnchor        .constraint(equalToConstant: UIScreen.main.bounds.height * (2/3)),

        blueView.leadingAnchor      .constraint(equalTo: contentView.leadingAnchor),
        blueView.trailingAnchor     .constraint(equalTo: contentView.trailingAnchor),
        blueView.topAnchor          .constraint(equalTo: productDescription.bottomAnchor),
        blueView.bottomAnchor       .constraint(equalTo: contentView.bottomAnchor),
        blueView.heightAnchor       .constraint(equalToConstant: UIScreen.main.bounds.height * (2/3))
        
        ])
    }
}

