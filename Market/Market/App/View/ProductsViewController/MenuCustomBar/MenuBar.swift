//
//  MenuBar.swift
//  Market
//
//  Created by Sergey on 10.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class MenuBar: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource  {

    let cellID = "cellID"
    let imageNames = ["information", "comments"]
//    var delegateToHomeController: CommentsCollectionViewController?
    var delegateToHomeController: DescriptionAndCommentsViewController?

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        collectionViewBarButtons.register(MenuCell.self, forCellWithReuseIdentifier: cellID)
        collectionViewBarButtons.selectItem(at: NSIndexPath(row: 0, section: 0) as IndexPath, animated: false, scrollPosition: [])
        self.setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private(set) lazy var collectionViewBarButtons: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: self.frame, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.scrollIndicatorInsets = UIEdgeInsets(top: self.frame.size.height, left: 0, bottom: 0, right: 0)
        cv.delegate = self
        cv.dataSource = self
        return cv
    }()
    
    
    //MARK: - CollectionViewSettings
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! MenuCell
        cell.imageView.image = UIImage(named: imageNames[indexPath.item])?.withRenderingMode(.alwaysTemplate)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        scrollToMenuIndex(menuIndex: 1)
        delegateToHomeController?.scrollToMenuIndex(menuIndex: indexPath.item)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: frame.width / 2, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    private func setupLayout() {
        self.addSubview(collectionViewBarButtons)
        
        NSLayoutConstraint.activate([
            
            self.collectionViewBarButtons.leftAnchor   .constraint(equalTo: self.safeAreaLayoutGuide.leftAnchor),
            self.collectionViewBarButtons.topAnchor      .constraint(equalTo: self.safeAreaLayoutGuide.topAnchor),
            self.collectionViewBarButtons.heightAnchor   .constraint(equalToConstant: 50),
            self.collectionViewBarButtons.widthAnchor    .constraint(equalTo: self.safeAreaLayoutGuide.widthAnchor)
        
        ])
    }

}
