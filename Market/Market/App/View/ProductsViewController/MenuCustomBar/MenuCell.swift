//
//  MenuCell.swift
//  Market
//
//  Created by Sergey on 10.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class MenuCell: BaseCell {

    
    private(set) lazy var textLabel: UILabel = {
       let label = UILabel()
        return label
    }()
    
    lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "information")?.withRenderingMode(.alwaysTemplate)
        image.tintColor = .lightGray
        image.translatesAutoresizingMaskIntoConstraints = false
        return image
    }()
    
    override var isHighlighted: Bool {
        didSet {
            imageView.tintColor = isHighlighted ? UIColor.red : UIColor.lightGray
        }
    }
    
    override var isSelected: Bool {
        didSet {
            imageView.tintColor = isSelected ? UIColor.red : UIColor.lightGray
        }
    }
    
    override func setupViews() {
        self.addSubview(imageView)
        self.contentMode = .center
        NSLayoutConstraint.activate([

            self.imageView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            self.imageView.topAnchor.constraint(equalTo: self.topAnchor),
            self.imageView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            self.imageView.widthAnchor.constraint(equalToConstant: self.frame.size.height)
        
        ])

    }
}
