//
//  ProductModel.swift
//  Market
//
//  Created by Sergey on 05.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import UIKit


class ProductModel: UIViewController {
    
    weak var coordinator: MainCoordinator?
    
    let requestFactory = RequestFactory()

    private var productView: ProductView  {
        return self.view as! ProductView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func loadView() {
        super.loadView()
        self.view = ProductView()
    }
    
}
