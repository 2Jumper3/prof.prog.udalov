//
//  ProductView.swift
//  Market
//
//  Created by Sergey on 05.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class ProductView: UIView {
    
    private(set) lazy var productName: UILabel = {
        let description = UILabel()
        description.translatesAutoresizingMaskIntoConstraints = false
        description.textColor = .black
        description.font = UIFont.boldSystemFont(ofSize: 20)
        description.numberOfLines = 50
        description.adjustsFontSizeToFitWidth = true
        description.text = "Here will be plased product name"
        description.textAlignment = .center
        return description
    }()
    
   
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupLayout()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupLayout()
    }
    private func setupLayout() {
        self.addSubview(productName)
        
        let topAnchorContraint: CGFloat = UIScreen.main.bounds.height * (1/4)
        
        NSLayoutConstraint.activate([
            self.productName.leftAnchor     .constraint(equalTo: self.leftAnchor),
            self.productName.widthAnchor    .constraint(equalTo: self.widthAnchor),
            self.productName.topAnchor      .constraint(equalTo: self.topAnchor),
            self.productName.bottomAnchor   .constraint(equalTo: self.topAnchor, constant: topAnchorContraint),
            
            
        ])
    }
}
