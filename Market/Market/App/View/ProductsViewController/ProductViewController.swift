//
//  ProductViewController.swift
//  Market
//
//  Created by Sergey on 05.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class ProductViewController: UIViewController {
    
    
    let layout = UICollectionViewFlowLayout()
    
    
    
    lazy var menuBar = MenuBar()
    lazy var productVC = ProductModel()
    lazy var commentsVC = DescriptionAndCommentsViewController(collectionViewLayout: layout)
    weak var coordinator: MainCoordinator?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.isHidden = false
        addVKVC()
        menuBar.delegateToHomeController = self.commentsVC
    }
    
    private func addVKVC() {
        self.addChild(self.productVC)
        self.addChild(self.commentsVC)
        self.view.addSubview(self.productVC.view)
        self.view.addSubview(self.commentsVC.view)
        self.view.addSubview(self.menuBar)
        self.productVC.didMove(toParent: self)
        self.commentsVC.didMove(toParent: self)
        self.productVC.view.translatesAutoresizingMaskIntoConstraints = false
        self.commentsVC.view.translatesAutoresizingMaskIntoConstraints = false
        self.menuBar.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            self.productVC.view.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.productVC.view.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
            self.productVC.view.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            self.productVC.view.heightAnchor.constraint(equalToConstant: self.view.frame.height / 4),
            
            self.menuBar.bottomAnchor.constraint(equalTo: self.productVC.view.bottomAnchor),
            self.menuBar.leftAnchor.constraint(equalTo: self.productVC.view.leftAnchor),
            self.menuBar.rightAnchor.constraint(equalTo: self.productVC.view.rightAnchor),
            self.menuBar.heightAnchor.constraint(equalToConstant: 50),
            
            self.commentsVC.view.topAnchor.constraint(equalTo: self.menuBar.bottomAnchor),
            self.commentsVC.view.leftAnchor.constraint(equalTo: self.productVC.view.leftAnchor),
            self.commentsVC.view.rightAnchor.constraint(equalTo: self.productVC.view.rightAnchor),
            self.commentsVC.view.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }

    

}
