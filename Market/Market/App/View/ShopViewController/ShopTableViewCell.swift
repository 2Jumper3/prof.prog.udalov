//
//  ShopTableViewCell.swift
//  Market
//
//  Created by Sergey on 06.03.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class ShopTableViewCell: UITableViewCell {

    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.productPriceLabel.font = UIFont.italicSystemFont(ofSize: 10)
    }

    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    func configure(productName: String, productPrice: String) {
        self.productPriceLabel?.text = productPrice
        self.productNameLabel?.text = productName
    }
        override func prepareForReuse() {
            super.prepareForReuse()
        }
}
