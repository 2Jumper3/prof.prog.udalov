//
//  ShopViewController.swift
//  Market
//
//  Created by Sergey on 25.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class ShopViewController: UIViewController {

    lazy var shopTableView = ShopViewModel()
    weak var coordinator: MainCoordinator?
    var totalGoodsModel: [Product] = []
    private var tableView: UITableView!

    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Shop"
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .white
        setupLayout()
        downloadGoods()
    }

    
    private func setupLayout() {
        tableView = UITableView()
        view.addSubview(tableView)
        tableView.register(ShopTableViewCell.self, forCellReuseIdentifier: "ShopTableViewCell")
        let xib = UINib(nibName: "ShopTableViewCell", bundle: Bundle.main)
        tableView.register(xib, forCellReuseIdentifier: "ShopTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.translatesAutoresizingMaskIntoConstraints = false
        let topAnchorConstant: CGFloat = 150

        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: topAnchorConstant),
            tableView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor),
            tableView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            tableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }
    
    private func downloadGoods() {
        self.shopTableView.download { (response) in
            self.totalGoodsModel = response
            DispatchQueue.main.async {
            self.tableView.reloadData()
            }
            print("total goods \(self.totalGoodsModel)")
        }
    }
    
}

extension ShopViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            
            return totalGoodsModel.count
        
        }
    


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShopTableViewCell", for: indexPath) as! ShopTableViewCell
        cell.configure(productName: totalGoodsModel[indexPath.row].product_name, productPrice: String(totalGoodsModel[indexPath.row].price))
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let vc = ProductViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
