//
//  ShopViewControllerModel.swift
//  Market
//
//  Created by Sergey on 25.02.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import UIKit

class ShopViewModel: UIViewController {

    weak var coordinator: MainCoordinator?
    var array: [Product] = []

    let requestFactory = RequestFactory()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    override func loadView() {
        super.loadView()
    }

}


extension ShopViewModel {
    func download(completion: @escaping ([Product]) -> ()) {
        let getGoods = requestFactory.getGoods()
        getGoods.getNewGoods(pageNumber: 1, id_category: 1) { response in
            switch response.result {
            case .success(let goods):
                for index in goods.products {
                    let product = Product(product_name: index.product_name, id: index.id, price: index.price)
                    self.array.append(product)
                    completion(self.array)
                    print("array \(self.array)")
                }
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}



