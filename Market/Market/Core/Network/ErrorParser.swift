//
//  ErrorParser.swift
//  Market
//
//  Created by Sergey on 20.01.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation

class ErrorParser: AbstractErrorParser {
    func parse(_ result: Error) -> Error {
        return result
    }
    
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
        return error
    }
}
