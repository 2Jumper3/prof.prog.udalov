//
//  MarketTests.swift
//  MarketTests
//
//  Created by Sergey on 20.01.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import XCTest
import Alamofire
@testable import Market

class MarketTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}

class ResponseResult: XCTestCase {

    
        let expectation = XCTestExpectation(description: "testing all responses")
        var errorParser: ErrorParserStub!

        override func setUp() {
            super.setUp()
            errorParser = ErrorParserStub()
        }
        
        override func tearDown() {
            super.tearDown()
            errorParser = nil
        }
        
        func testLoginResponse() {
            Alamofire
                .request("https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/login.json")
                .responseCodable(errorParser: errorParser) { [weak self] (response: DataResponse<LoginResult>) in
                    switch response.result {
                    case .success(_): break
                    case .failure:
                        XCTFail()
                    }
                    self?.expectation.fulfill()
            }
            wait(for: [expectation], timeout: 10.0)
        }
    
    func testLogoutResponse() {
        Alamofire
//            .request("https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/logout.json")
            .request("http://0.0.0.0:8080/logout")

            .responseCodable(errorParser: errorParser) { [weak self] (response: DataResponse<LogOutResult>) in
                switch response.result {
                case .success(_): break
                case .failure:
                    XCTFail()
                }
                self?.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testRegisterResponse() {
        Alamofire
            .request("https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/registerUser.json")
            .responseCodable(errorParser: errorParser) { [weak self] (response: DataResponse<RigesterResult>) in
                switch response.result {
                case .success(_): break
                case .failure:
                    XCTFail()
                }
                self?.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testGetGoodsResponse() {
        Alamofire
            .request("https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/catalogData.json")
            .responseCodable(errorParser: errorParser) { [weak self] (response: DataResponse<[GoodsList]>) in
                switch response.result {
                case .success(_): break
                case .failure:
                    XCTFail()
                }
                self?.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    
    func testGetCardResponse() {
        Alamofire
            .request("https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/getBasket.json")
            .responseCodable(errorParser: errorParser) { [weak self] (response: DataResponse<CardList>) in
                switch response.result {
                case .success(_): break
                case .failure:
                    XCTFail()
                }
                self?.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }
    func testNewComment() {
        Alamofire
            .request("https://raw.githubusercontent.com/GeekBrainsTutorial/online-store-api/master/responses/addReview.json")
            .responseCodable(errorParser: errorParser) { [weak self] (response: DataResponse<NewCommentResponse>) in
                switch response.result {
                case .success(_): break
                case .failure:
                    XCTFail()
                }
                self?.expectation.fulfill()
        }
        wait(for: [expectation], timeout: 10.0)
    }

  
}
