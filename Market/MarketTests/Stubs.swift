//
//  Stubs.swift
//  MarketTests
//
//  Created by Sergey on 21.01.2020.
//  Copyright © 2020 Sergey. All rights reserved.
//

import Foundation
import Alamofire
@testable import Market


enum ApiErrorStub: Error {
    case fatalError
}


struct ErrorParserStub: AbstractErrorParser {
    func parse(_ result: Error) -> Error {
        return ApiErrorStub.fatalError
    }
    
    func parse(response: HTTPURLResponse?, data: Data?, error: Error?) -> Error? {
        return error
    }
}
