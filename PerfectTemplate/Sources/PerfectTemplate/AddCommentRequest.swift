//
//  File.swift
//  
//
//  Created by Sergey on 06.02.2020.
//

import Foundation


struct AddCommentRequest {
    
    var id_user = 0
    var text = ""

    init(_ json: [String: AnyObject]) {
      
        if let id_user = json["id_user"] as? Int {
            self.id_user = id_user
        }
        
        if let text = json["text"] as? String {
            self.text = text
        }

    }
}
