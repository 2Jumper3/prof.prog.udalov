//
//  File.swift
//  
//
//  Created by Sergey on 06.02.2020.
//

import Foundation

struct AddToBasketRequest {
    
    var id_user = 0
    var id_product = 0
    var quantity = 0

    init(_ json: [String: AnyObject]) {
      
        if let id_user = json["id_user"] as? Int {
            self.id_user = id_user
        }
        if let id_product = json["id_product"] as? Int {
            self.id_product = id_product
        }
        if let quantity = json["quantity"] as? Int {
            self.quantity = quantity
        }
    }
}
