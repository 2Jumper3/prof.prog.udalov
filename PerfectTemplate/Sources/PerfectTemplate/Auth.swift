//
//  File.swift
//  
//
//  Created by Sergey on 29.01.2020.
//

import Foundation
import PerfectHTTP

class RequestController {
    
    let register: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Wrong user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let registerRequest = RegisterRequest(json)
            print("Request - \(registerRequest)")

            try response.setBody(json: ["result": 1, "userMessage": "Регистрация прошла успешно!"])
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
    let login: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Wrong user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let loginRequest = LoginRequest(json)
            print("Request - \(loginRequest)")

            try response.setBody(json: ["result": 1, "user": ["id_user" : 123, "user_login" : "geekbrains", "user_name" : "John", "user_lastname" : "Doe", "bio" : "Place your bio here", "credit_card" : "1234-1234-1234-1234"]])
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
    let getInfo: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Wrong user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let loginRequest = GetInfoRequest(json)
            print("Request - \(loginRequest)")

            try response.setBody(json: ["id_user" : 123, "user_login" : "geekbrains", "user_name" : "John", "user_lastname" : "Doe", "bio" : "Place your bio here", "credit_card" : "1234-1234-1234-1234"])
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
    let logout: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Wrong user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let logoutRequest = LogoutRequest(json)
            print("Request - \(logoutRequest)")


            try response.setBody(json: ["result": 1, "userMessage": "До новый встреч!"])
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
    let getGoods: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Wrong user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let getGoodsRequest = GetGoodsRequest(json)
            print("Request - \(getGoodsRequest)")


            try response.setBody(json: ["page_number" : 1, "products" : [["id_product" : 123, "product_name" : "Ноутбук", "price" : 45600],["id_product" : 133, "product_name" : "Мышка", "price" : 1000]]])
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
    let getBasket: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Wrong user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let getBasketRequest = GetBasketRequest(json)
            print("Request - \(getBasketRequest)")


            try response.setBody(json: ["amount" : 46600, "countGoods" : 2, "contents" : [["id_product" : 123, "product_name" : "Ноутбук", "price" : 45600, "quantity" : 1],["id_product" : 133, "product_name" : "Мышка", "price" : 1000, "quantity" : 1]]])
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
    let addComment: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Wrong user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let addCommentRequest = AddCommentRequest(json)
            print("Request - \(addCommentRequest)")


            try response.setBody(json: ["result": 1, "userMessage": "Ваш комментарий был отправлен на модерацию"])
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
    let showComments: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Wrong user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let showAllComments = ShowCommentsRequest(json)
            print("Request - \(showAllComments)")


            try response.setBody(json: ["comments" : [["name": "Tanya", "comment": "Spasibo za tovar"],
                ["name": "Masha", "comment": "Ogonek"],
                ["name": "Katya", "comment": "Perfect"],
                ["name": "Petya", "comment": "Lychshie"],
                ["name": "Vanya", "comment": "Mama ya v Dubae"],
                ["name": "Sasha", "comment": "TOzhe ho4y v Dybae"]]])
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }
    
    let addToBasket: (HTTPRequest, HTTPResponse) -> () =  { request, response in
        guard let str = request.postBodyString, let data = str.data(using: .utf8) else {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Wrong user data"))
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: AnyObject]
            let addToBasket = AddToBasketRequest(json)
            print("Request - \(addToBasket)")


            try response.setBody(json: ["text" : "Товар был успешно добавлен в корзину!"])
            response.completed()
        } catch {
            response.completed(status: HTTPResponseStatus.custom(code: 500, message: "Parse data error - \(error)"))
        }
    }


    
}
