//
//  File.swift
//  
//
//  Created by Sergey on 06.02.2020.
//

import Foundation


struct GetBasketRequest {
    
    var id_user = 0

    init(_ json: [String: AnyObject]) {
      
        if let id_user = json["id_user"] as? Int {
            self.id_user = id_user
        }
    }
}
