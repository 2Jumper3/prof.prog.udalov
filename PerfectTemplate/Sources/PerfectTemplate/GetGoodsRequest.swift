//
//  File.swift
//  
//
//  Created by Sergey on 05.02.2020.
//

import Foundation

struct GetGoodsRequest {
    
    var pageNumber: Int = 1
    var id_category = 1

    
    init(_ json: [String: AnyObject]) {
      
        if let pageNumber = json["page_number"] as? Int {
            self.pageNumber = pageNumber
        }
        if let idCategory = json["id_category"] as? Int {
            self.id_category = idCategory
        }
    }
}
