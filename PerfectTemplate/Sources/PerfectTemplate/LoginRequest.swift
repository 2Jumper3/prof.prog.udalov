//
//  File.swift
//  
//
//  Created by Sergey on 29.01.2020.
//

import Foundation


struct LoginRequest {
    
    var username: String = ""
    var password: String = ""

    
    init(_ json: [String: AnyObject]) {
      
        if let username = json["username"] as? String {
            self.username = username
            
        }
        if let password = json["password"] as? String {
            self.password = password
        }
    }
}
