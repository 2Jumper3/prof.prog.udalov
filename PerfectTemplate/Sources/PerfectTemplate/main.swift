//
//  main.swift
//  PerfectTemplate
//
//  Created by Kyle Jessup on 2015-11-05.
//	Copyright (C) 2015 PerfectlySoft, Inc.
//
//===----------------------------------------------------------------------===//
//
// This source file is part of the Perfect.org open source project
//
// Copyright (c) 2015 - 2016 PerfectlySoft Inc. and the Perfect project authors
// Licensed under Apache License v2.0
//
// See http://perfect.org/licensing.html for license information
//
//===----------------------------------------------------------------------===//
//

import PerfectHTTP
import PerfectHTTPServer

let server = HTTPServer()
let requestController = RequestController()
var routes = Routes()

routes.add(method: .post, uri: "/register", handler: requestController.register)
routes.add(method: .post, uri: "/login", handler: requestController.login)
routes.add(method: .post, uri: "/logout", handler: requestController.logout)
routes.add(method: .post, uri: "/getGoods", handler: requestController.getGoods)
routes.add(method: .post, uri: "/getBasket", handler: requestController.getBasket)
routes.add(method: .post, uri: "/addComment", handler: requestController.addComment)
routes.add(method: .post, uri: "/showComments", handler: requestController.showComments)
routes.add(method: .post, uri: "/addToBasket", handler: requestController.addToBasket)
routes.add(method: .post, uri: "/userinfo", handler: requestController.getInfo)

server.addRoutes(routes)
server.serverPort = 8080

do {
    try server.start()
} catch {
    fatalError("Network error - \(error)")
}


//MARK: - Реквесты для insomnia:


/*
 login:
 {
 "username" : "Somebody",
 "password" : "mypassword"
 }
 
 logout:
 
 {
 "id_user": 123
 }
 
 Register:
 
 {
 "id_user": 123,
 "username": "Somebody",
 "password": "mypassword",
 "email": "some@some.ru",
 "gender": "m",
 "credit_card": "9872389-2424-234224-234",
 "bio": "This is good! I think I will switch to another language"
 }
 
 */
